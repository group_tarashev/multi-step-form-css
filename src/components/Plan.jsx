import React from 'react'
import Outer from './Outer'
import Cards from './Cards'

const Plan = ({inputs, setInputs}) => {
  return (
    <>
        <Outer head={'Select you plan'} parag={'You have the option of monthly or yearly billing.'} children={<Cards inputs={inputs} setInputs={setInputs}/>}/>
    </>
  )
}

export default Plan