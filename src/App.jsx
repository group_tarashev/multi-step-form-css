import { useEffect, useState } from "react";
import "./styles/App.css";
import Sidebar from "./components/Sidebar";
import RightSide from "./components/RightSide";

function App() {
  const [inputs, setInputs] = useState({
    name: '',
    email: '',
    phone: '',
    isMonth: true,
    isActive: 0,
    plan: "Arcade",
    planPrice: 9,
    online: 0,
    storage: null,
    custom: null,
    select: null,
  });
  const [checkedName, setCheckedName] = useState({
    online: null,
    storage: null,
    custom: null,
  });
  
  return (
    <section className="App">
      <div className="content">
        <img
          src="./bg-sidebar-mobile.svg"
          className="image-mb"
          alt=""
        />

        <Sidebar inputs={inputs} />
        <RightSide
          inputs={inputs}
          setInputs={setInputs}
          checkedName={checkedName}
          setCheckedName={setCheckedName}
        />
      </div>
    </section>
  );
}

export default App;
