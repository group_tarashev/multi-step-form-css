import React from "react";
import "../styles/sidebar.css";
const tabs = ["your info", "select plan", "add-ons", "summary"];
const Sidebar = ({inputs}) => {
    const {isActive} = inputs
  const content = (
      <ul className="side-list">
      {tabs.map((item, i) => (
        <li key={i}>
            <span className={isActive === i ? "number active" : "number"}>
                {i + 1}
            </span>
            <div className="desc">
                <p>Step{ i+1}</p>
                <span>{item.toUpperCase()}</span>
            </div>
        </li>
      ))}
    </ul>
  );
  return <div className="sidebar">
    {content}
  </div>;
};

export default Sidebar;
