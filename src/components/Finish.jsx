import React from "react";
import "../styles/finish.css";
import Outer from "./Outer";
const Finish = ({ inputs, setInputs }) => {
  const { isMonth, plan, planPrice,online,custom,storage } = inputs;
  const total = parseInt(planPrice) + (online !==null && parseInt(online)) +(custom !==null && parseInt(custom)) + (storage !== null && parseInt(storage));
  
  const handleChange = () =>{
    setInputs(prev => ({...prev, isActive: 1}))
  }
  const content = (
    <div className="finish">
      <div className="fn-container">
        <div className="finish-cont">
          <div className="finish-title">
            <p>
              {plan} ({isMonth ? "Monthly" : "Yearly"})
            </p>
            <span onClick={handleChange}>change</span>
          </div>
          <h5 className="finish-price">
            ${planPrice} {isMonth ? "/mo" : "/yr"}
          </h5>
        </div>
        { (online !== 0 || custom !== 0 || storage !== 0) &&<div className="hr" />}
        <div className="finish-add-cont">
          {online !== null && <div className="finish-add">
            <p>Online service</p>
            <span>+${online}{isMonth ?"/mo": '/yr'}</span>
          </div>}
          {storage !== null &&<div className="finish-add">
            <p>Larger storage</p>
            <span>+${storage}{isMonth ?"/mo": '/yr'}</span>
          </div>}
          {custom !== null && <div className="finish-add">
            <p>Customaizable profile</p>
            <span>+${custom}{isMonth ?"/mo": '/yr'}</span>
          </div>}
        </div>
      </div>
      <div className="total-price">
        <p>Total ({isMonth ? 'per month' : 'per year'})</p>
        <span>+${total}{isMonth ?"/mo": '/yr'}</span>
      </div>
    </div>
  );
  return (
    <div className="finish">
      <Outer
        head={"Finishing up"}
        parag={"Double-check everything looks OK before confirming"}
        children={content}
      />
    </div>
  );
};

export default Finish;
