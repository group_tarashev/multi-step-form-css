import React from 'react'
import Outer from './Outer'
import Form from './Form'

const Personal = ({inputs, setInputs}) => {
  return (
    <>
        <Outer head={"Personal info"} parag={'Please provide your name, email address, and phone number.'} children={<Form inputs={inputs} setInputs={setInputs}/>} />
    </>
    
  )
}

export default Personal