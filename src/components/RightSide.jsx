import React, { useEffect, useState } from "react";
import Personal from "./Personal";
import "../styles/right.css";
import Plan from "./Plan";
import AddOns from "./AddOns";
import Finish from "./Finish";
import Thank from "./Thank";
const RightSide = ({ inputs, setInputs, checkedName, setCheckedName }) => {
    const [checkInputs, setCheckInputs] = useState(false)
    useEffect(() => {
        if (
          inputs.email.length !== 0 &&
          inputs.name.length !== 0 &&
          inputs.phone.length !== 0 &&
          inputs.email.includes("@")
        ) {
          setCheckInputs(true);
        } else {
          setCheckInputs(false);
        }
      }, [inputs]);
  return (
    <div className="right">
      {inputs.isActive === 0 && (
        <Personal inputs={inputs} setInputs={setInputs} />
      )}
      {inputs.isActive === 1 && <Plan inputs={inputs} setInputs={setInputs} />}
      {inputs.isActive === 2 && (
        <AddOns inputs={inputs} setInputs={setInputs} checkedName={checkedName} setCheckedName={setCheckedName}/>
      )}
      {inputs.isActive === 3 && (
        <Finish inputs={inputs} setInputs={setInputs} />
      )}
      {
        inputs.isActive === 4 && (
            <Thank/>
        )
      }
      <div className="btn-group">
        {inputs.isActive === 0 ||
          (inputs.isActive < 4 && (
            <button
              onClick={() =>
                setInputs((prev) => ({ ...prev, isActive: prev.isActive - 1 }))
              }
              className="btn-back"
              style={{opacity: inputs.isActive === 0 ? 0 : 1}}
            >
              Go Back
            </button>
          ))}
        {inputs.isActive > 3 || inputs.select === -1 || (
          <button
            onClick={() =>
              setInputs((prev) => ({ ...prev, isActive: prev.isActive + 1 }))
            }
            className="btn-next"
          >
            Next Step
          </button>
        )}
        {inputs.isActive === 3 && <button onClick={() => setInputs(prev => ({...prev, isActive: 4}))} className="btn-conf">Confirm</button>}
      </div>
    </div>
  );
};

export default RightSide;
