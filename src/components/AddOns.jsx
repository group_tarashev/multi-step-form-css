import React from "react";
import Outer from "./Outer";
import "../styles/addons.css";
const addons = [
  {
    title: "Online service",
    subTitle: "Access to multiplayer games",
    priceMonth: 1,
    priceYear: 10,
    name: "online",
  },
  {
    title: "Larger storage",
    subTitle: "Extra 1TB of cloud save",
    priceMonth: 2,
    priceYear: 20,
    name: "storage",
  },
  {
    title: "Customizable profile",
    subTitle: "Custom theme on your profile",
    priceMonth: 2,
    priceYear: 20,
    name: "custom",
  },
];
const AddOns = ({ inputs, setInputs, checkedName, setCheckedName }) => {
  const { isMonth, online, custom, storage } = inputs;
  const handleChange = (e, i) => {
    const { name, value, checked } = e.target;
    const input = { [name]: value };
    const inputChecked = { [name]: i };
    const initialInput = {[name]: null}
    const initialInputChecked = {[name]: null}
    if (checked) {
      setInputs((prev) => ({ ...prev, ...input }));
      setCheckedName(prev => ({...prev, ...inputChecked}))
    }else{
      setInputs((prev) => ({ ...prev, ...initialInput }));
      setCheckedName(prev => ({...prev, ...initialInputChecked}))
    }
  };

  const content = (
    <div className="addons">
      <ul>
        {addons.map((item, i) => (
          <li key={i} style={{
            backgroundColor: (online || storage || custom) !== 0 && (checkedName.online === i || checkedName.storage === i || checkedName.custom ===i ) &&  '#acc8e6'
          }}>
            <div className="add-cont">
              <input
                type="checkbox"
                onChange={(e) => handleChange(e, i)}
                name={item.name}
                value={isMonth ? item.priceMonth : item.priceYear}
                className="add-check"
                checked={(inputs.online ||inputs.storage || inputs.custom) !== 0 && (checkedName.online === i || checkedName.storage === i || checkedName.custom === i) && true }
              />
              <div className="add-name">
                <p>{item.title}</p>
                <span>{item.subTitle}</span>
              </div>
            </div>
            <p className="add-price">+${isMonth ? item.priceMonth  + "/mo": item.priceYear + '/yr'}</p>
          </li>
        ))}
      </ul>
    </div>
  );
  return (
    <div>
      <Outer
        head={"Pick add-ons"}
        parag={"Add-ons help enchance your gaming experience."}
        children={content}
      />
    </div>
  );
};

export default AddOns;
