import React from "react";
import "../styles/card.css";

const cards = [
  {
    icon: "./icon-arcade.svg",
    title: "Arcade",
    monthPrice: 9,
    yearPrice: 90,
  },
  {
    icon: "./icon-advanced.svg",
    title: "Advanced",
    monthPrice: 12,
    yearPrice: 120,
  },
  {
    icon: "./icon-pro.svg",
    title: "Pro",
    monthPrice: 15,
    yearPrice: 150,
  },
];

const Cards = ({ inputs, setInputs }) => {
    const {isMonth} = inputs
  const handleSelect = (i) => {
    setInputs((prev) => ({ ...prev, select: i, plan: cards[i].title, planPrice: isMonth ? cards[i].monthPrice : cards[i].yearPrice }));
  };
  const handleMonth = () =>{
    setInputs(prev =>({...prev, select: -1}))
    setInputs((prev) => ({ ...prev, isMonth: !prev.isMonth }));
  }
  return (
    <div className="cards">
      <ul className="cards-items">
        {cards.map((item, i) => (
          <li
            onClick={() => handleSelect(i)}
            key={i}
            style={{ backgroundColor: inputs.select === i ? "#acc8e6" : "", borderColor: inputs.select === i ? "blue" : "" }}
          >
            <img src={item.icon} alt="" className="img"/>
            <div className="card-content">
              <p>{item.title}</p>
              <span className="card-price">${ isMonth ? item.monthPrice+'/mo' : item.yearPrice+'/yr'}</span>
              <span>{isMonth || "2 month free"}</span>
            </div>
          </li>
        ))}
      </ul>
      <div className="switch-btn">
        <span className="card-mothly">Monthly</span>
        <div className="switch" onClick={handleMonth}>
          <span className={isMonth ? "toggler" : ' toggler toggler-active'}></span>
        </div>
        <span className="card-yearly">Yearly</span>
      </div>
    </div>
  );
};

export default Cards;
