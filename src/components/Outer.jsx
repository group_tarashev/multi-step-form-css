import React from 'react'
import '../styles/outer.css'
const Outer = ({head, parag, children}) => {
  return (
    <section className='outer'>
        <div className="head">
            <h2 className="title">{head}</h2>
            <p className='sub-title'>{parag}</p>
        </div>
        {children}
        
    </section>
  )
}

export default Outer