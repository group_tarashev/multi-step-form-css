import React from 'react'
import '../styles/thank.css'
const Thank = () => {
  return (
    <div className='thank'>
        <img src="./icon-thank-you.svg" alt="" />
        <h2>Thank you</h2>
        <p>Thanks for confiming your subscribtion! We hope you have fun using our platform. If you ever need support, please feel free to email us at iliyan.tarashev@gmail.com</p>
    </div>
  )
}

export default Thank