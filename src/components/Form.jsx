import React from "react";
import "../styles/form.css";
const Form = ({ inputs, setInputs }) => {
  const handleChange = (e) => {
    const { name, value } = e.target;
    const input = {[name] : value};
    setInputs(prev =>({...prev, ...input}))
  };
  return (
    <div className="form">
      <div className="form-name">
        <label htmlFor="name">Name</label>
        <input
          id="name"
          type="text"
          name="name"
          placeholder="e.g. John Doe"
          className="name"
          value={inputs.name}
          onChange={handleChange}
        />
      </div>
      <div className="form-email">
        <label htmlFor="email">Email</label>
        <input
          type="email"
          placeholder="e.g. johndoe@gmail.com"
          name="email"
          id="phone"
          className="email"
          value={inputs.email}
          onChange={handleChange}
        />
      </div>
      <div className="form-phone">
        <label htmlFor="phone">Phone Number</label>
        <input
          type="text"
          name="phone"
          placeholder="e.g. +1 234 567 890"
          id="phone"
          className="phone"
          value={inputs.phone}
          onChange={handleChange}
        />
      </div>
    </div>
  );
};

export default Form;
